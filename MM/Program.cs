﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MM
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }        
    public class Grain
    {
        public int id { set; get; }
        public Color color { set; get; }
          
        public Grain()
        {
            id = 0;
            color = Color.White;
        }
        public void setValuesFromARGB(int ARGBV)
        {
            id = ARGBV;
            color = Color.FromArgb(ARGBV);
        }
        public void setValuesFromColor(Color colorV)
        {
            id = colorV.ToArgb();
            color = colorV;
        }
    }
}
