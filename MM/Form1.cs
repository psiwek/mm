using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MM
{
    public partial class Form1 : Form
    {
        public const int ASIZE = 300;

        static Bitmap mainBitmap;
        static Grain[,] currentArray, futureArray;
        static Random rnd = new Random();
        static List<Color> excludedColorL = new List<Color>();

        public Form1()
        {
            InitializeComponent();
            mainBitmap = new Bitmap(ASIZE, ASIZE, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            currentArray = prepareArray(ASIZE);
            excludedColorL.Add(Color.Black);
        }

        private Grain[,] prepareArray(int size)
        {
            Grain[,] array = new Grain[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = new Grain();
                }
            }
            return array;
        }

        private void updateMainBitmap()
        {
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    mainBitmap.SetPixel(i, j, currentArray[i, j].color);
                }
            }
            pictureBox1.Image = mainBitmap;
            pictureBox1.Update();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            button1.Enabled = true;

            //Inclusions
            if (checkBox1.Checked && checkBox4.Checked)
            {
                int inclusionSize = Int32.Parse(textBox2.Text);
                int inclusions = Int32.Parse(textBox3.Text);

                if (radioButton7.Checked)
                {
                    for (int i = 0; i < inclusions; i++)
                    {
                        int x, y;
                        x = rnd.Next(ASIZE - inclusionSize);
                        y = rnd.Next(ASIZE - inclusionSize);
                        for (int k = x; k < x + inclusionSize; k++)
                        {
                            for (int l = y; l < y + inclusionSize; l++)
                            {
                                currentArray[k, l].setValuesFromColor(Color.Black);
                            }
                        }

                    }
                }
                else if (radioButton8.Checked)
                {
                    int[,] inclusionXY = new int[2, inclusions];
                    for (int i = 0; i < inclusions; i++)
                    {
                        int x, y;
                        inclusionXY[0, i] = x = rnd.Next(ASIZE - inclusionSize);
                        inclusionXY[1, i] = y = rnd.Next(ASIZE - inclusionSize);
                        currentArray[x, y].setValuesFromColor(Color.Black);
                    }
                    for (int i = 0; i < ASIZE; i++)
                    {
                        for (int j = 0; j < ASIZE; j++)
                        {
                            for (int k = 0; k < inclusions; k++)
                            {
                                double distance = Math.Sqrt(Math.Pow(i - inclusionXY[0, k], 2) + Math.Pow(j - inclusionXY[1, k], 2));
                                if (distance <= inclusionSize / 2.0)
                                {
                                    currentArray[i, j].setValuesFromColor(Color.Black);
                                }
                            }
                        }
                    }
                }
            }

            //Seeds
            int seeds = Int32.Parse(textBox1.Text);
            for (int i = 0; i < seeds; i++)
            {
                int x, y;
                x = rnd.Next(ASIZE);
                y = rnd.Next(ASIZE);
                if (currentArray[x, y].color != Color.White)
                {
                    i--;
                    continue;
                }
                currentArray[x, y].setValuesFromColor(Color.FromArgb(rnd.Next(254), rnd.Next(254), rnd.Next(254)));
            }
            updateMainBitmap();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            bool cont = false;
            do
            {
                cont = false;
                futureArray = prepareArray(ASIZE);
                for (int i = 0; i < ASIZE; i++)
                {
                    for (int j = 0; j < ASIZE; j++)
                    {
                        if (currentArray[i, j].id == 0)
                        {
                            Dictionary<int, int> neigbours = new Dictionary<int, int>();
                            if (radioButton4.Checked)
                            {
                                //absorbingBC
                                if (radioButton1.Checked)
                                {
                                    //Von Neumann                                   
                                    if (i - 1 > -1 && currentArray[i - 1, j].id != 0 && !isColorExcluded(currentArray[i - 1, j].color)) { updateDictionary(ref neigbours, i - 1, j); }
                                    if (j - 1 > -1 && currentArray[i, j - 1].id != 0 && !isColorExcluded(currentArray[i, j - 1].color)) { updateDictionary(ref neigbours, i, j - 1); }
                                    if (i + 1 < ASIZE && currentArray[i + 1, j].id != 0 && !isColorExcluded(currentArray[i + 1, j].color)) { updateDictionary(ref neigbours, i + 1, j); }
                                    if (j + 1 < ASIZE && currentArray[i, j + 1].id != 0 && !isColorExcluded(currentArray[i, j + 1].color)) { updateDictionary(ref neigbours, i, j + 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        int maxKey = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                        futureArray[i, j].setValuesFromARGB(maxKey);
                                    }
                                    else
                                    {
                                        cont = true;
                                    }
                                }
                                else if (radioButton2.Checked)
                                {
                                    //Moore
                                    if (i - 1 > -1 && currentArray[i - 1, j].id != 0 && !isColorExcluded(currentArray[i - 1, j].color)) { updateDictionary(ref neigbours, i - 1, j); }
                                    if (j - 1 > -1 && currentArray[i, j - 1].id != 0 && !isColorExcluded(currentArray[i, j - 1].color)) { updateDictionary(ref neigbours, i, j - 1); }
                                    if (i + 1 < ASIZE && currentArray[i + 1, j].id != 0 && !isColorExcluded(currentArray[i + 1, j].color)) { updateDictionary(ref neigbours, i + 1, j); }
                                    if (j + 1 < ASIZE && currentArray[i, j + 1].id != 0 && !isColorExcluded(currentArray[i, j + 1].color)) { updateDictionary(ref neigbours, i, j + 1); }

                                    if (i - 1 > -1 && j - 1 > -1 && currentArray[i - 1, j - 1].id != 0 && !isColorExcluded(currentArray[i - 1, j - 1].color)) { updateDictionary(ref neigbours, i - 1, j - 1); }
                                    if (i + 1 < ASIZE && j + 1 < ASIZE && currentArray[i + 1, j + 1].id != 0 && !isColorExcluded(currentArray[i + 1, j + 1].color)) { updateDictionary(ref neigbours, i + 1, j + 1); }
                                    if (i - 1 > -1 && j + 1 < ASIZE && currentArray[i - 1, j + 1].id != 0 && !isColorExcluded(currentArray[i - 1, j + 1].color)) { updateDictionary(ref neigbours, i - 1, j + 1); }
                                    if (i + 1 < ASIZE && j - 1 > -1 && currentArray[i + 1, j - 1].id != 0 && !isColorExcluded(currentArray[i + 1, j - 1].color)) { updateDictionary(ref neigbours, i + 1, j - 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        int maxKey = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                        futureArray[i, j].setValuesFromARGB(maxKey);
                                    }
                                    else
                                    {
                                        cont = true;
                                    }
                                }
                                else if (radioButton5.Checked)
                                {
                                    //More with SC
                                    Dictionary<int, int> neigboursNearest = new Dictionary<int, int>();
                                    Dictionary<int, int> neigboursFurther = new Dictionary<int, int>();

                                    if (i - 1 > -1 && currentArray[i - 1, j].id != 0 && !isColorExcluded(currentArray[i - 1, j].color)) { updateDictionary(ref neigbours, i - 1, j); updateDictionary(ref neigboursNearest, i - 1, j); }
                                    if (j - 1 > -1 && currentArray[i, j - 1].id != 0 && !isColorExcluded(currentArray[i, j - 1].color)) { updateDictionary(ref neigbours, i, j - 1); updateDictionary(ref neigboursNearest, i, j - 1); }
                                    if (i + 1 < ASIZE && currentArray[i + 1, j].id != 0 && !isColorExcluded(currentArray[i + 1, j].color)) { updateDictionary(ref neigbours, i + 1, j); updateDictionary(ref neigboursNearest, i + 1, j); }
                                    if (j + 1 < ASIZE && currentArray[i, j + 1].id != 0 && !isColorExcluded(currentArray[i, j + 1].color)) { updateDictionary(ref neigbours, i, j + 1); updateDictionary(ref neigboursNearest, i, j + 1); }

                                    if (i - 1 > -1 && j - 1 > -1 && currentArray[i - 1, j - 1].id != 0 && !isColorExcluded(currentArray[i - 1, j - 1].color)) { updateDictionary(ref neigbours, i - 1, j - 1); updateDictionary(ref neigboursFurther, i - 1, j - 1); }
                                    if (i + 1 < ASIZE && j + 1 < ASIZE && currentArray[i + 1, j + 1].id != 0 && !isColorExcluded(currentArray[i + 1, j + 1].color)) { updateDictionary(ref neigbours, i + 1, j + 1); updateDictionary(ref neigboursFurther, i + 1, j + 1); }
                                    if (i - 1 > -1 && j + 1 < ASIZE && currentArray[i - 1, j + 1].id != 0 && !isColorExcluded(currentArray[i - 1, j + 1].color)) { updateDictionary(ref neigbours, i - 1, j + 1); updateDictionary(ref neigboursFurther, i - 1, j + 1); }
                                    if (i + 1 < ASIZE && j - 1 > -1 && currentArray[i + 1, j - 1].id != 0 && !isColorExcluded(currentArray[i + 1, j - 1].color)) { updateDictionary(ref neigbours, i + 1, j - 1); updateDictionary(ref neigboursFurther, i + 1, j - 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        //1
                                        int maxValue = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Value;
                                        int maxKey;
                                        if (maxValue >= 5)
                                        {
                                            maxKey = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].setValuesFromARGB(maxKey);
                                            continue;
                                        }
                                        //2
                                        maxValue = neigboursNearest.OrderByDescending(x => x.Value).FirstOrDefault().Value;
                                        if (maxValue >= 3)
                                        {
                                            maxKey = neigboursNearest.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].setValuesFromARGB(maxKey);
                                            continue;
                                        }
                                        //3
                                        maxValue = neigboursFurther.OrderByDescending(x => x.Value).FirstOrDefault().Value;
                                        if (maxValue >= 3)
                                        {
                                            maxKey = neigboursFurther.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].setValuesFromARGB(maxKey);
                                            continue;
                                        }
                                        //4
                                        int randomN = rnd.Next(0, 99);
                                        if (randomN <= (Int32.Parse(textBox4.Text) - 1))
                                        {
                                            maxKey = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].setValuesFromARGB(maxKey);
                                            continue;
                                        }
                                        else
                                        {
                                            cont = true;
                                        }
                                    }
                                    else
                                    {
                                        cont = true;
                                    }


                                }
                            }
                            else if (radioButton3.Checked)
                            {
                                //periodicBC
                                if (radioButton1.Checked)
                                {
                                    //Von Neumann
                                    if (i - 1 > -1 && currentArray[i - 1, j].id != 0 && !isColorExcluded(currentArray[i - 1, j].color)) { updateDictionary(ref neigbours, i - 1, j); }
                                    if (i - 1 == -1 && currentArray[ASIZE - 1, j].id != 0 && !isColorExcluded(currentArray[ASIZE - 1, j].color)) { updateDictionary(ref neigbours, ASIZE - 1, j); }
                                    if (j - 1 > -1 && currentArray[i, j - 1].id != 0 && !isColorExcluded(currentArray[i, j - 1].color)) { updateDictionary(ref neigbours, i, j - 1); }
                                    if (j - 1 == -1 && currentArray[i, ASIZE - 1].id != 0 && !isColorExcluded(currentArray[i, ASIZE - 1].color)) { updateDictionary(ref neigbours, i, ASIZE - 1); }
                                    if (i + 1 < ASIZE && currentArray[i + 1, j].id != 0 && !isColorExcluded(currentArray[i + 1, j].color)) { updateDictionary(ref neigbours, i + 1, j); }
                                    if (i + 1 == ASIZE && currentArray[0, j].id != 0 && !isColorExcluded(currentArray[0, j].color)) { updateDictionary(ref neigbours, 0, j); }
                                    if (j + 1 < ASIZE && currentArray[i, j + 1].id != 0 && !isColorExcluded(currentArray[i, j + 1].color)) { updateDictionary(ref neigbours, i, j + 1); }
                                    if (j + 1 == ASIZE && currentArray[i, 0].id != 0 && !isColorExcluded(currentArray[i, 0].color)) { updateDictionary(ref neigbours, i, 0); }

                                    if (neigbours.Count > 0)
                                    {
                                        int maxKey = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                        futureArray[i, j].setValuesFromARGB(maxKey);
                                    }
                                    else
                                    {
                                        cont = true;
                                    }
                                }
                                else if (radioButton2.Checked)
                                {
                                    //Moore
                                    if (i - 1 > -1 && currentArray[i - 1, j].id != 0 && !isColorExcluded(currentArray[i - 1, j].color)) { updateDictionary(ref neigbours, i - 1, j); }
                                    if (i - 1 == -1 && currentArray[ASIZE - 1, j].id != 0 && !isColorExcluded(currentArray[ASIZE - 1, j].color)) { updateDictionary(ref neigbours, ASIZE - 1, j); }
                                    if (j - 1 > -1 && currentArray[i, j - 1].id != 0 && !isColorExcluded(currentArray[i, j - 1].color)) { updateDictionary(ref neigbours, i, j - 1); }
                                    if (j - 1 == -1 && currentArray[i, ASIZE - 1].id != 0 && !isColorExcluded(currentArray[i, ASIZE - 1].color)) { updateDictionary(ref neigbours, i, ASIZE - 1); }
                                    if (i + 1 < ASIZE && currentArray[i + 1, j].id != 0 && !isColorExcluded(currentArray[i + 1, j].color)) { updateDictionary(ref neigbours, i + 1, j); }
                                    if (i + 1 == ASIZE && currentArray[0, j].id != 0 && !isColorExcluded(currentArray[0, j].color)) { updateDictionary(ref neigbours, 0, j); }
                                    if (j + 1 < ASIZE && currentArray[i, j + 1].id != 0 && !isColorExcluded(currentArray[i, j + 1].color)) { updateDictionary(ref neigbours, i, j + 1); }
                                    if (j + 1 == ASIZE && currentArray[i, 0].id != 0 && !isColorExcluded(currentArray[i, 0].color)) { updateDictionary(ref neigbours, i, 0); }

                                    if (i - 1 > -1 && j - 1 > -1 && currentArray[i - 1, j - 1].id != 0 && !isColorExcluded(currentArray[i - 1, j - 1].color)) { updateDictionary(ref neigbours, i - 1, j - 1); }
                                    if (i - 1 == -1 && j - 1 == -1 && currentArray[ASIZE - 1, ASIZE - 1].id != 0 && !isColorExcluded(currentArray[ASIZE - 1, ASIZE - 1].color)) { updateDictionary(ref neigbours, ASIZE - 1, ASIZE - 1); }
                                    if (i + 1 < ASIZE && j + 1 < ASIZE && currentArray[i + 1, j + 1].id != 0 && !isColorExcluded(currentArray[i + 1, j + 1].color)) { updateDictionary(ref neigbours, i + 1, j + 1); }
                                    if (i + 1 == ASIZE && j + 1 == ASIZE && currentArray[0, 0].id != 0 && !isColorExcluded(currentArray[0, 0].color)) { updateDictionary(ref neigbours, 0, 0); }
                                    if (i - 1 > -1 && j + 1 < ASIZE && currentArray[i - 1, j + 1].id != 0 && !isColorExcluded(currentArray[i - 1, j + 1].color)) { updateDictionary(ref neigbours, i - 1, j + 1); }
                                    if (i - 1 == -1 && j + 1 == ASIZE && currentArray[ASIZE - 1, 0].id != 0 && !isColorExcluded(currentArray[ASIZE - 1, 0].color)) { updateDictionary(ref neigbours, ASIZE - 1, 0); }
                                    if (i + 1 < ASIZE && j - 1 > -1 && currentArray[i + 1, j - 1].id != 0 && !isColorExcluded(currentArray[i + 1, j - 1].color)) { updateDictionary(ref neigbours, i + 1, j - 1); }
                                    if (i + 1 == ASIZE && j - 1 == -1 && currentArray[0, ASIZE - 1].id != 0 && !isColorExcluded(currentArray[0, ASIZE - 1].color)) { updateDictionary(ref neigbours, 0, ASIZE - 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        int maxKey = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                        futureArray[i, j].setValuesFromARGB(maxKey);
                                    }
                                    else
                                    {
                                        cont = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            futureArray[i, j] = currentArray[i, j];
                        }
                    }
                }
                currentArray = futureArray;
                if (checkBox3.Checked)
                {
                    updateMainBitmap();
                }
            } while (cont);

            if (checkBox1.Checked && checkBox5.Checked)
            {
                int inclusionSize = Int32.Parse(textBox2.Text);
                int inclusions = Int32.Parse(textBox3.Text);

                if (radioButton7.Checked)
                {
                    for (int i = 0; i < inclusions; i++)
                    {
                        int x, y;
                        while (true)
                        {
                            x = rnd.Next(1, ASIZE - inclusionSize);
                            y = rnd.Next(1, ASIZE - inclusionSize);
                            Grain core = currentArray[x, y];
                            if (currentArray[x - 1, y].color != core.color ||
                               currentArray[x + 1, y].color != core.color ||
                               currentArray[x, y - 1].color != core.color ||
                               currentArray[x, y + 1].color != core.color)
                            {
                                break;
                            }
                        }


                        for (int k = (int)Math.Floor(x - inclusionSize / 2.0); k < (int)Math.Floor(x - inclusionSize / 2.0) + inclusionSize; k++)
                        {
                            for (int l = (int)Math.Floor(y - inclusionSize / 2.0); l < (int)Math.Floor(y - inclusionSize / 2.0) + inclusionSize; l++)
                            {
                                currentArray[k, l].setValuesFromColor(Color.Black);
                            }
                        }

                    }
                }
                else if (radioButton8.Checked)
                {
                    int[,] inclusionXY = new int[2, inclusions];
                    for (int i = 0; i < inclusions; i++)
                    {
                        int x, y;
                        while (true)
                        {
                            inclusionXY[0, i] = x = rnd.Next(1, ASIZE - inclusionSize);
                            inclusionXY[1, i] = y = rnd.Next(1, ASIZE - inclusionSize);
                            Grain core = currentArray[x, y];
                            if (currentArray[x - 1, y].color != core.color ||
                               currentArray[x + 1, y].color != core.color ||
                               currentArray[x, y - 1].color != core.color ||
                               currentArray[x, y + 1].color != core.color)
                            {
                                break;
                            }
                        }
                        currentArray[x, y].setValuesFromColor(Color.Black);
                    }
                    for (int i = 0; i < ASIZE; i++)
                    {
                        for (int j = 0; j < ASIZE; j++)
                        {
                            for (int k = 0; k < inclusions; k++)
                            {
                                double distance = Math.Sqrt(Math.Pow(i - inclusionXY[0, k], 2) + Math.Pow(j - inclusionXY[1, k], 2));
                                if (distance <= inclusionSize / 2.0)
                                {
                                    currentArray[i, j].setValuesFromColor(Color.Black);
                                }
                            }
                        }
                    }
                }
            }
            updateMainBitmap();
        }

        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sdialog = new SaveFileDialog();
            sdialog.Filter = "PNG File|*.BMP";
            if (sdialog.ShowDialog() == DialogResult.OK)
            {
                mainBitmap.Save(sdialog.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
            }
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sdialog = new SaveFileDialog();
            sdialog.Filter = "Text File|*.TXT";
            if (sdialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter outfile = new StreamWriter(sdialog.FileName);
                outfile.WriteLine("300,300");
                for (int i = 0; i < ASIZE; i++)
                {
                    for (int j = 0; j < ASIZE; j++)
                    {
                        outfile.WriteLine(i.ToString() + ',' + j.ToString() + ',' + currentArray[i, j].id.ToString());
                    }
                }
                outfile.Close();
            }
        }

        private void imageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog odialog = new OpenFileDialog();
            odialog.Filter = "Image File|*.BMP";
            if (odialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap tmpBitmap = new Bitmap(odialog.FileName);
                for (int i = 0; i < ASIZE; i++)
                {
                    for (int j = 0; j < ASIZE; j++)
                    {
                        currentArray[i, j].color = tmpBitmap.GetPixel(i, j);
                        currentArray[i, j].id = tmpBitmap.GetPixel(i, j).ToArgb();
                    }
                }
                updateMainBitmap();
            }
        }

        private void textToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog odialog = new OpenFileDialog();
            odialog.Filter = "Text File|*.TXT";
            if (odialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader infile = new StreamReader(odialog.FileName);
                if (infile.ReadLine() == "300,300")
                {
                    while (!infile.EndOfStream)
                    {
                        string line = infile.ReadLine();
                        string[] parts = line.Split(',');
                        if (parts.Length != 3)
                        {
                            return;
                        }
                        int i = Int32.Parse(parts[0]);
                        int j = Int32.Parse(parts[1]);
                        int id = Int32.Parse(parts[2]);
                        Color color = Color.FromArgb(id);
                        currentArray[i, j].id = id;
                        currentArray[i, j].color = color;
                    }
                }
                updateMainBitmap();
            }
        }

        private void updateDictionary(ref Dictionary<int, int> d, int i, int j)
        {
            d.TryGetValue(currentArray[i, j].color.ToArgb(), out int value);
            d[currentArray[i, j].color.ToArgb()] = value + 1;
        }

        private bool isColorExcluded(Color colorToCheck)
        {
            return excludedColorL.Contains(colorToCheck);
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            textBox4.Enabled = !textBox4.Enabled;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (checkBox2.Checked || checkBox7.Checked)
            {
                int x = (int)Math.Floor(e.X / 2.0);
                int y = (int)Math.Floor(e.Y / 2.0);
                excludedColorL.Add(mainBitmap.GetPixel(x, y));
                Console.WriteLine(mainBitmap.GetPixel(x, y) + "????" + currentArray[x, y].color);
            }
            else if (checkBox6.Checked)
            {
                int x = (int)Math.Floor(e.X / 2.0);
                int y = (int)Math.Floor(e.Y / 2.0);
                excludedColorL.Add(mainBitmap.GetPixel(x, y));
                Console.WriteLine(mainBitmap.GetPixel(x, y) + "????" + currentArray[x, y].color);

                Color newColor = Color.FromArgb(rnd.Next(254), rnd.Next(254), rnd.Next(254));

                for (int i = 0; i < ASIZE; i++)
                {
                    for (int j = 0; j < ASIZE; j++)
                    {
                        if (isColorExcluded(currentArray[i, j].color))
                        {
                            currentArray[i, j].setValuesFromColor(newColor);
                        }
                    }
                }

                excludedColorL.Clear();
                excludedColorL.Add(Color.Black);
                excludedColorL.Add(newColor);

            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            int size = Int32.Parse(textBox6.Text);
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    if (checkBox7.Checked && !isColorExcluded(currentArray[i, j].color))
                    {
                        continue;
                    }
                    if (i - 1 > -1 && currentArray[i - 1, j].id != currentArray[i, j].id && !isColorExcluded(currentArray[i - 1, j].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }
                    if (j - 1 > -1 && currentArray[i, j - 1].id != currentArray[i, j].id && !isColorExcluded(currentArray[i, j - 1].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }
                    if (i + 1 < ASIZE && currentArray[i + 1, j].id != currentArray[i, j].id && !isColorExcluded(currentArray[i + 1, j].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }
                    if (j + 1 < ASIZE && currentArray[i, j + 1].id != currentArray[i, j].id && !isColorExcluded(currentArray[i, j + 1].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }

                    if (i - 1 > -1 && j - 1 > -1 && currentArray[i - 1, j - 1].id != currentArray[i, j].id && !isColorExcluded(currentArray[i - 1, j - 1].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }
                    if (i + 1 < ASIZE && j + 1 < ASIZE && currentArray[i + 1, j + 1].id != currentArray[i, j].id && !isColorExcluded(currentArray[i + 1, j + 1].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }
                    if (i - 1 > -1 && j + 1 < ASIZE && currentArray[i - 1, j + 1].id != currentArray[i, j].id && !isColorExcluded(currentArray[i - 1, j + 1].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }
                    if (i + 1 < ASIZE && j - 1 > -1 && currentArray[i + 1, j - 1].id != currentArray[i, j].id && !isColorExcluded(currentArray[i + 1, j - 1].color)) { for (int x = 0; x < size; x++) { if (i - x <= 0 || j - x <= 0) continue; currentArray[i - x, j - x].setValuesFromColor(Color.Black); } }                   
                }
            }
            updateMainBitmap();
            float GB = 0.0000000f;
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    if (currentArray[i, j].color == Color.Black)
                    {
                        GB += 1.0f;
                    }
                }
            }
            GB /= (ASIZE * ASIZE);
            GB *= 100.0f;
            GB += 0.0000001f;
            textBox5.Text = GB.ToString().Substring(0, 6) + " %";
            if (checkBox7.Checked)
            {
                excludedColorL.Clear();
                excludedColorL.Add(Color.Black);
            }
            updateMainBitmap();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button2.Enabled = true;
            button1.Enabled = false;
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    if (isColorExcluded(currentArray[i, j].color))
                    {
                    }
                    else
                    {
                        currentArray[i, j].id = 0;
                        currentArray[i, j].color = Color.White;
                    }
                }
            }
            updateMainBitmap();
        }
    }
}
