﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MM2
{
    public partial class Form1 : Form
    {
        const int ASIZE = 300;
        enum BitmapState { microstructure, energy };

        Bitmap mainBitmap;
        Bitmap color_dBitmap;
        Grain[,] mainGrainA;
        Random rnd;
        List<Color> grainColors;
        List<Color> preservedColors;

        BitmapState bs;
        public Form1()
        {
            InitializeComponent();
            mainGrainA = new Grain[ASIZE, ASIZE];
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    mainGrainA[i, j] = new Grain();
                }
            }
            rnd = new Random();
            bs = BitmapState.microstructure;
            grainColors = new List<Color>();
            preservedColors = new List<Color>();
        }

        private Bitmap CreateBitmap(Grain[,] grainA)
        {
            Bitmap bitmap = new Bitmap(ASIZE, ASIZE, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, ASIZE, ASIZE), ImageLockMode.ReadWrite, bitmap.PixelFormat);
            unsafe
            {
                byte* Spointer = (byte*)data.Scan0;
                const int bytesInRow = ASIZE * 4;

                Parallel.For(0, ASIZE, i =>
                 {
                     byte* Cpointer = Spointer + i * bytesInRow;
                     for (int j = 0; j < ASIZE; j++)
                     {
                         Cpointer[0] = grainA[i, j].gColor.B;
                         Cpointer[1] = grainA[i, j].gColor.G;
                         Cpointer[2] = grainA[i, j].gColor.R;
                         Cpointer[3] = grainA[i, j].gColor.A;

                         Cpointer += 4;
                     }
                 });
            }
            bitmap.UnlockBits(data);
            return bitmap;
        }

        private Bitmap CreateEnergyBitmap(Grain[,] grainA)
        {
            Bitmap bitmap = new Bitmap(ASIZE, ASIZE, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, ASIZE, ASIZE), ImageLockMode.ReadWrite, bitmap.PixelFormat);
            unsafe
            {
                int energyInside = Int32.Parse(textBox5.Text);
                int energyOnEdges = Int32.Parse(textBox6.Text);

                byte* Spointer = (byte*)data.Scan0;
                const int bytesInRow = ASIZE * 4;

                for (int i = 0; i < ASIZE; i++)
                {
                    byte* Cpointer = Spointer + i * bytesInRow;
                    for (int j = 0; j < ASIZE; j++)
                    {
                        Color c;
                        if (grainA[i, j].energy == energyInside)
                        {
                            c = Color.YellowGreen;
                        }
                        else if (grainA[i, j].energy == energyOnEdges)
                        {
                            c = Color.DarkSlateBlue;
                        }
                        else
                        {
                            c = Color.Red;
                        }
                        Cpointer[0] = c.B;
                        Cpointer[1] = c.G;
                        Cpointer[2] = c.R;
                        Cpointer[3] = c.A;

                        Cpointer += 4;
                    }
                }
            }
            bitmap.UnlockBits(data);
            return bitmap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CARadioButton.Checked)
            {
                //Seeds
                int seeds = Int32.Parse(textBox3.Text);
                for (int i = 0; i < seeds; i++)
                {
                    int x, y;
                    x = rnd.Next(ASIZE);
                    y = rnd.Next(ASIZE);
                    if (mainGrainA[x, y].gColor != Color.White)
                    {
                        i--;
                        continue;
                    }
                    mainGrainA[x, y].gColor = Color.FromArgb(0, rnd.Next(256), rnd.Next(256));
                }
                Grain[,] futureArray;
                bool cont = false;
                do
                {
                    cont = false;
                    futureArray = new Grain[ASIZE, ASIZE];
                    for (int i = 0; i < ASIZE; i++)
                    {
                        for (int j = 0; j < ASIZE; j++)
                        {
                            futureArray[i, j] = new Grain();
                        }
                    }

                    for (int i = 0; i < ASIZE; i++)
                    {
                        for (int j = 0; j < ASIZE; j++)
                        {
                            if (mainGrainA[i, j].gColor == Color.White)
                            {
                                Dictionary<Color, int> neigbours = new Dictionary<Color, int>();
                                if (radioButton1.Checked)
                                {
                                    //Von Neumann                                   
                                    if (i - 1 > -1 && mainGrainA[i - 1, j].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j].gColor)) { UpdateDictionary(ref neigbours, i - 1, j); }
                                    if (j - 1 > -1 && mainGrainA[i, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i, j - 1].gColor)) { UpdateDictionary(ref neigbours, i, j - 1); }
                                    if (i + 1 < ASIZE && mainGrainA[i + 1, j].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j].gColor)) { UpdateDictionary(ref neigbours, i + 1, j); }
                                    if (j + 1 < ASIZE && mainGrainA[i, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i, j + 1].gColor)) { UpdateDictionary(ref neigbours, i, j + 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        Color maxColor = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                        futureArray[i, j].gColor = maxColor;
                                        cont = true;
                                    }
                                }
                                else if (radioButton2.Checked)
                                {
                                    //Moore
                                    if (i - 1 > -1 && mainGrainA[i - 1, j].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j].gColor)) { UpdateDictionary(ref neigbours, i - 1, j); }
                                    if (j - 1 > -1 && mainGrainA[i, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i, j - 1].gColor)) { UpdateDictionary(ref neigbours, i, j - 1); }
                                    if (i + 1 < ASIZE && mainGrainA[i + 1, j].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j].gColor)) { UpdateDictionary(ref neigbours, i + 1, j); }
                                    if (j + 1 < ASIZE && mainGrainA[i, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i, j + 1].gColor)) { UpdateDictionary(ref neigbours, i, j + 1); }

                                    if (i - 1 > -1 && j - 1 > -1 && mainGrainA[i - 1, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j - 1].gColor)) { UpdateDictionary(ref neigbours, i - 1, j - 1); }
                                    if (i + 1 < ASIZE && j + 1 < ASIZE && mainGrainA[i + 1, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j + 1].gColor)) { UpdateDictionary(ref neigbours, i + 1, j + 1); }
                                    if (i - 1 > -1 && j + 1 < ASIZE && mainGrainA[i - 1, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j + 1].gColor)) { UpdateDictionary(ref neigbours, i - 1, j + 1); }
                                    if (i + 1 < ASIZE && j - 1 > -1 && mainGrainA[i + 1, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j - 1].gColor)) { UpdateDictionary(ref neigbours, i + 1, j - 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        Color maxColor = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                        futureArray[i, j].gColor = maxColor;
                                        cont = true;
                                    }
                                }
                                else if (radioButton5.Checked)
                                {
                                    //More with SC
                                    Dictionary<Color, int> neigboursNearest = new Dictionary<Color, int>();
                                    Dictionary<Color, int> neigboursFurther = new Dictionary<Color, int>();

                                    if (i - 1 > -1 && mainGrainA[i - 1, j].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j].gColor)) { UpdateDictionary(ref neigbours, i - 1, j); UpdateDictionary(ref neigboursNearest, i - 1, j); }
                                    if (j - 1 > -1 && mainGrainA[i, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i, j - 1].gColor)) { UpdateDictionary(ref neigbours, i, j - 1); UpdateDictionary(ref neigboursNearest, i, j - 1); }
                                    if (i + 1 < ASIZE && mainGrainA[i + 1, j].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j].gColor)) { UpdateDictionary(ref neigbours, i + 1, j); UpdateDictionary(ref neigboursNearest, i + 1, j); }
                                    if (j + 1 < ASIZE && mainGrainA[i, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i, j + 1].gColor)) { UpdateDictionary(ref neigbours, i, j + 1); UpdateDictionary(ref neigboursNearest, i, j + 1); }

                                    if (i - 1 > -1 && j - 1 > -1 && mainGrainA[i - 1, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j - 1].gColor)) { UpdateDictionary(ref neigbours, i - 1, j - 1); UpdateDictionary(ref neigboursFurther, i - 1, j - 1); }
                                    if (i + 1 < ASIZE && j + 1 < ASIZE && mainGrainA[i + 1, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j + 1].gColor)) { UpdateDictionary(ref neigbours, i + 1, j + 1); UpdateDictionary(ref neigboursFurther, i + 1, j + 1); }
                                    if (i - 1 > -1 && j + 1 < ASIZE && mainGrainA[i - 1, j + 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i - 1, j + 1].gColor)) { UpdateDictionary(ref neigbours, i - 1, j + 1); UpdateDictionary(ref neigboursFurther, i - 1, j + 1); }
                                    if (i + 1 < ASIZE && j - 1 > -1 && mainGrainA[i + 1, j - 1].gColor != Color.White && !preservedColors.Contains(mainGrainA[i + 1, j - 1].gColor)) { UpdateDictionary(ref neigbours, i + 1, j - 1); UpdateDictionary(ref neigboursFurther, i + 1, j - 1); }

                                    if (neigbours.Count > 0)
                                    {
                                        //1
                                        int maxValue = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Value;
                                        Color maxColor;
                                        if (maxValue >= 5)
                                        {
                                            maxColor = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].gColor = maxColor;
                                            cont = true;
                                            continue;
                                        }
                                        //2
                                        maxValue = neigboursNearest.OrderByDescending(x => x.Value).FirstOrDefault().Value;
                                        if (maxValue >= 3)
                                        {
                                            maxColor = neigboursNearest.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].gColor = maxColor;
                                            cont = true;
                                            continue;
                                        }
                                        //3
                                        maxValue = neigboursFurther.OrderByDescending(x => x.Value).FirstOrDefault().Value;
                                        if (maxValue >= 3)
                                        {
                                            maxColor = neigboursFurther.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].gColor = maxColor;
                                            cont = true;
                                            continue;
                                        }
                                        //4
                                        int randomN = rnd.Next(0, 99);
                                        if (randomN <= (Int32.Parse(textBox4.Text) - 1))
                                        {
                                            maxColor = neigbours.OrderByDescending(x => x.Value).FirstOrDefault().Key;
                                            futureArray[i, j].gColor = maxColor;
                                            cont = true;
                                            continue;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                futureArray[i, j] = mainGrainA[i, j];
                            }
                        }
                    }
                    mainGrainA = futureArray;

                    mainBitmap = CreateBitmap(mainGrainA);
                    pictureBox1.Image = mainBitmap;
                    pictureBox1.Update();
                    bs = BitmapState.microstructure;
                } while (cont);
            }
            else if (MCRadioButton.Checked)
            {
                int numberOfStates = Int32.Parse(textBox1.Text);
                for (int i = 0; i < numberOfStates; i++)
                {
                    grainColors.Add(Color.FromArgb(0, rnd.Next(256), rnd.Next(256)));
                }

                for (int i = 0; i < ASIZE; i++)
                {
                    for (int j = 0; j < ASIZE; j++)
                    {
                        int v = rnd.Next(0, numberOfStates);
                        if (mainGrainA[i, j].gColor != Color.White)
                        {
                            continue;
                        }
                        mainGrainA[i, j].gColor = grainColors[v];
                    }
                }
                mainBitmap = CreateBitmap(mainGrainA);
                pictureBox1.Image = mainBitmap;
                pictureBox1.Update();
                bs = BitmapState.microstructure;

                int mcs = Int32.Parse(textBox2.Text);
                for (int k = 0; k < mcs; k++)
                {
                    List<XY> mcsList = new List<XY>();
                    for (int i = 0; i < ASIZE; i++)
                    {
                        for (int j = 0; j < ASIZE; j++)
                        {
                            mcsList.Add(new XY(i, j));
                        }
                    }

                    while (mcsList.Count > 0)
                    {
                        int tick = rnd.Next(mcsList.Count);
                        XY currentPos = mcsList[tick];
                        int x = currentPos.x;
                        int y = currentPos.y;
                        mcsList.RemoveAt(tick);
                        Dictionary<Color, int> neighborhood = new Dictionary<Color, int>();
                        if (x - 1 > -1) { UpdateDictionary(ref neighborhood, x - 1, y); }
                        if (x + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x + 1, y); }
                        if (y - 1 > -1) { UpdateDictionary(ref neighborhood, x, y - 1); }
                        if (y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x, y + 1); }
                        if (x - 1 > -1 && y - 1 > -1) { UpdateDictionary(ref neighborhood, x - 1, y - 1); }
                        if (x + 1 < ASIZE && y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x + 1, y + 1); }
                        if (x - 1 > -1 && y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x - 1, y + 1); }
                        if (x + 1 < ASIZE && y - 1 > -1) { UpdateDictionary(ref neighborhood, x + 1, y - 1); }

                        double energyBefore = 0;
                        double energyAfter = 0;

                        for (int i = 0; i < neighborhood.Count; i++)
                        {
                            if (neighborhood.ElementAt(i).Key == mainGrainA[x, y].gColor) { continue; }
                            energyBefore += neighborhood.ElementAt(i).Value;
                        }
                        Color selectedColor = neighborhood.ElementAt(rnd.Next(neighborhood.Count)).Key;
                        for (int i = 0; i < neighborhood.Count; i++)
                        {
                            if (neighborhood.ElementAt(i).Key == selectedColor) { continue; }
                            energyAfter += neighborhood.ElementAt(i).Value;
                        }
                        double delta = energyAfter - energyBefore;
                        if (delta <= 0) { mainGrainA[x, y].gColor = selectedColor; }
                    }

                    mainBitmap = CreateBitmap(mainGrainA);
                    pictureBox1.Image = mainBitmap;
                    pictureBox1.Update();
                    bs = BitmapState.microstructure;
                }
            }
            calculateGrainEnergy();
        }

        public void UpdateDictionary(ref Dictionary<Color, int> d, int i, int j)
        {
            d.TryGetValue(mainGrainA[i, j].gColor, out int value);
            d[mainGrainA[i, j].gColor] = value + 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    if (preservedColors.Contains(mainGrainA[i, j].gColor)) { continue; }
                    mainGrainA[i, j] = new Grain();
                }
            }
            grainColors.Clear();

            mainBitmap = CreateBitmap(mainGrainA);
            pictureBox1.Image = mainBitmap;
            pictureBox1.Update();
            bs = BitmapState.microstructure;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            int x = (int)Math.Floor(e.X / 2.0);
            int y = (int)Math.Floor(e.Y / 2.0);
            Console.WriteLine("X:" + e.X + " Y:" + e.Y);
            preservedColors.Add(mainGrainA[x, y].gColor);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (bs == BitmapState.microstructure)
            {
                color_dBitmap = CreateEnergyBitmap(mainGrainA);
                pictureBox1.Image = color_dBitmap;
                pictureBox1.Update();
                bs = BitmapState.energy;
                button1.Text = "Show microstructure";
            }
            else if (bs == BitmapState.energy)
            {
                mainBitmap = CreateBitmap(mainGrainA);
                pictureBox1.Image = mainBitmap;
                pictureBox1.Update();
                bs = BitmapState.microstructure;
                button1.Text = "Show energy distribution";
            }

        }
        private void calculateGrainEnergy()
        {
            for (int x = 0; x < ASIZE; x++)
            {
                for (int y = 0; y < ASIZE; y++)
                {
                    if (mainGrainA[x, y].gColor == Color.White) { continue; }

                    Dictionary<Color, int> neighborhood = new Dictionary<Color, int>();
                    if (x - 1 > -1) { UpdateDictionary(ref neighborhood, x - 1, y); }
                    if (x + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x + 1, y); }
                    if (y - 1 > -1) { UpdateDictionary(ref neighborhood, x, y - 1); }
                    if (y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x, y + 1); }
                    if (x - 1 > -1 && y - 1 > -1) { UpdateDictionary(ref neighborhood, x - 1, y - 1); }
                    if (x + 1 < ASIZE && y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x + 1, y + 1); }
                    if (x - 1 > -1 && y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x - 1, y + 1); }
                    if (x + 1 < ASIZE && y - 1 > -1) { UpdateDictionary(ref neighborhood, x + 1, y - 1); }

                    if (radioButton7.Checked == true)
                    {
                        if (neighborhood.Count == 1 && neighborhood.ElementAt(0).Key != Color.White)
                        {
                            mainGrainA[x, y].energy = Int32.Parse(textBox5.Text);
                        }
                        else if (neighborhood.Count > 1)
                        {
                            mainGrainA[x, y].energy = Int32.Parse(textBox6.Text);
                        }
                    }
                    else if (radioButton6.Checked == true)
                    {
                        mainGrainA[x, y].energy = Int32.Parse(textBox5.Text);
                    }
                }
            }

        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            uiGraying();
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            uiGraying();
        }

        private void radioButton10_CheckedChanged(object sender, EventArgs e)
        {
            uiGraying();
        }

        private void uiGraying()
        {
            if (radioButton9.Checked == true)
            {
                textBox7.Enabled = false;
                textBox9.Enabled = true;
            }
            else if (radioButton8.Checked == true)
            {
                textBox7.Enabled = true;
                textBox9.Enabled = true;
            }
            else if (radioButton10.Checked == true)
            {
                textBox7.Enabled = false;
                textBox9.Enabled = false;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            int numberOfNucleons = Int32.Parse(textBox8.Text);
            int increaseNumber = Int32.Parse(textBox7.Text);
            int nucleateInterval = Int32.Parse(textBox9.Text);
            bool onBoundaries = false;

            color_dBitmap = CreateEnergyBitmap(mainGrainA);

            if (radioButton3.Checked == true) onBoundaries = true;
            nucleate_Recrystalization(numberOfNucleons, onBoundaries);

            int j = 0;
            for (int i = 0; i < Int32.Parse(textBox10.Text); i++)
            {
                if (radioButton9.Checked || radioButton8.Checked)
                {
                    if (j == nucleateInterval)
                    {
                        j = 0;
                        if (radioButton8.Checked) { numberOfNucleons += increaseNumber; }
                        nucleate_Recrystalization(numberOfNucleons, onBoundaries);
                    }
                    else
                    {
                        j++;
                    }
                }
                grow_Recrystalization();
            }
        }

        private void grow_Recrystalization()
        {
            List<XY> mcsList = new List<XY>();
            for (int i = 0; i < ASIZE; i++)
            {
                for (int j = 0; j < ASIZE; j++)
                {
                    mcsList.Add(new XY(i, j));
                }
            }

            while (mcsList.Count > 0)
            {
                int tick = rnd.Next(mcsList.Count);
                XY currentPos = mcsList[tick];
                int x = currentPos.x;
                int y = currentPos.y;
                mcsList.RemoveAt(tick);

                Dictionary<Color, int> neighborhood = new Dictionary<Color, int>();
                if (x - 1 > -1) { UpdateDictionary(ref neighborhood, x - 1, y); }
                if (x + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x + 1, y); }
                if (y - 1 > -1) { UpdateDictionary(ref neighborhood, x, y - 1); }
                if (y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x, y + 1); }
                if (x - 1 > -1 && y - 1 > -1) { UpdateDictionary(ref neighborhood, x - 1, y - 1); }
                if (x + 1 < ASIZE && y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x + 1, y + 1); }
                if (x - 1 > -1 && y + 1 < ASIZE) { UpdateDictionary(ref neighborhood, x - 1, y + 1); }
                if (x + 1 < ASIZE && y - 1 > -1) { UpdateDictionary(ref neighborhood, x + 1, y - 1); }

                double energyBefore = 0;
                double energyAfter = 0;

                for (int i = 0; i < neighborhood.Count; i++)
                {
                    if (neighborhood.ElementAt(i).Key == mainGrainA[x, y].gColor) { continue; }
                    energyBefore += neighborhood.ElementAt(i).Value;
                }
                energyBefore += mainGrainA[x, y].energy;
                Color selectedColor = neighborhood.ElementAt(rnd.Next(neighborhood.Count)).Key;
                if (selectedColor.G != 0 || selectedColor.B != 0) { continue; }

                for (int i = 0; i < neighborhood.Count; i++)
                {
                    if (neighborhood.ElementAt(i).Key == selectedColor) { continue; }
                    energyAfter += neighborhood.ElementAt(i).Value;
                }
                double delta = energyAfter - energyBefore;
                if (delta < 0)
                {
                    mainGrainA[x, y].gColor = selectedColor;
                    mainGrainA[x, y].energy = 0;
                    color_dBitmap.SetPixel(x, y, Color.Red);
                }
            }

            mainBitmap = CreateBitmap(mainGrainA);
            pictureBox1.Image = mainBitmap;
            pictureBox1.Update();
            bs = BitmapState.microstructure;

        }

        private void nucleate_Recrystalization(int numberOfNucleons, bool onlyOnBoundaries = false)
        {
            int unstuck = 0;
            for (int i = 0; i < numberOfNucleons; i++)
            {
                if (unstuck >= 2 * numberOfNucleons) { return; }
                int x, y;
                x = rnd.Next(ASIZE);
                y = rnd.Next(ASIZE);
                if (onlyOnBoundaries)
                {
                    if (mainGrainA[x, y].energy != Int32.Parse(textBox6.Text))
                    {
                        i--;
                        unstuck++;
                        continue;
                    }
                }
                mainGrainA[x, y].energy = 0;
                mainGrainA[x, y].gColor = Color.FromArgb(rnd.Next(220), 0, 0);
            }
        }
    }

    public class Grain
    {
        public Color gColor;
        public int energy = 11;

        public Grain()
        {
            gColor = Color.White;
        }
        public Grain(Color gColor)
        {
            this.gColor = gColor;
        }
    }
    public class XY
    {
        public int x;
        public int y;

        public XY(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
